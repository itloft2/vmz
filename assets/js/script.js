$(document).ready(function() {



  $('.banner__list').owlCarousel({
    items:1,
    nav: true,
    loop: true,
    navText:['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.1 31.1" width="19.057" height="31.113" fill="#fff"><path d="M15.5 31.1l3.5-3.5-12-12 12.1-12L15.6 0 0 15.5l15.5 15.6z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="19.057" height="31.113" viewBox="0 0 19.057 31.113" fill="#fff"><path d="M3.5 0L0 3.5l12.057 12.057L0 27.613l3.5 3.5 15.557-15.556"/></svg>']
  });

  $('.slider__list').owlCarousel({
    items:1,
    nav: true,
    loop: true,
    navText:['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.1 31.1" width="19.057" height="31.113" fill="#fff"><path d="M15.5 31.1l3.5-3.5-12-12 12.1-12L15.6 0 0 15.5l15.5 15.6z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="19.057" height="31.113" viewBox="0 0 19.057 31.113" fill="#fff"><path d="M3.5 0L0 3.5l12.057 12.057L0 27.613l3.5 3.5 15.557-15.556"/></svg>'],
  });



  $('.search-btn').on('click', function(e) {
    e.preventDefault();
    console.log('ddd');
    $('.header').addClass('search-block');
  });

  $('.search .overlay').on('click', function(e) {
    e.preventDefault();
    $('.header').removeClass('search-block');
  });

  var myMap;

  // Дождёмся загрузки API и готовности DOM.
  ymaps.ready(init);

  function init () {
      myMap = new ymaps.Map('map', {
          center:[60.710232, 28.749404], // Выборг
          zoom:10
      });
  }
}());

$(window).on('load',function() {
  $('.lightgallery').lightGallery({
         selector: '.lg__photo',
         download: false
     });
});
