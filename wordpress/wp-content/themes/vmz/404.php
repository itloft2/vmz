<?php get_header(); ?>

<article class="error-page dark">
  <div class="container">
    <section class="error">
      <div class="error__num">404</div>
      <div class="error__text">
        <p>Неверно набран адрес или такой страницы на сайте не существует</p>
        <p><a href="/">Вернуться на главную</a></p>
      </div>
    </section>
  </div>
</article>

<?php get_footer(); ?>
