<?php //$fields = get_fields()['base']; ?>

    <?php if (have_rows('base')) : ?>

      <?php while (have_rows('base')) : the_row(); ?>

        <?php /*добавить слайдер с изображениями */ ?>
        <?php if (get_row_layout() == 'add_slider'): ?>
          <?php  $slider = get_sub_field('slider'); if(!empty($slider)) : ?>
            <div class="slider">
              <ul class="slider__list lightgallery">
                <?php foreach ($slider as $slide): ?>
                  <li class="slider__item" style="background-image:url('<?php echo $slide['url']?>')">
                    <a href="<?php echo $slide['url']?>" class="lg__photo"></a>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>

          <?php /*добавить  фотогалерея */ ?>
        <?php elseif (get_row_layout() == 'add_gallery'): ?>
            <?php  $gallery = get_sub_field('gallery'); if(!empty($gallery)) : ?>
              <div class="gallery lightgallery">
                  <?php foreach ($gallery as $photo): ?>
                    <a href="<?php echo $photo['url']?>" class="lg__photo gallery__item">
                      <img src="<?php echo $photo['url']?>" alt="" />
                    </a>
                  <?php endforeach; ?>
                </ul>
              </div>
            <?php endif; ?>

        <?php /*добавить текстовый блок */ ?>
        <?php elseif (get_row_layout() == "add_text"): ?>
          <div class="text"><?php the_sub_field('text') ?> </div>

        <?php /*добавить таблицу*/ ?>
      <?php elseif (get_row_layout() == "add_table"): ?>


      <?php $table = get_sub_field('table');
      if ( $table ) {
        echo '<table border="0" class="table"> <caption>';
        echo get_sub_field('caption');
        echo'</caption>';
        if ( $table['header'] ) { echo '<thead>'; echo '<tr>';
          foreach ( $table['header'] as $th ) { echo '<th>'; echo $th['c']; echo '</th>'; } echo '</tr>'; echo '</thead>'; } echo '<tbody>';
          foreach ( $table['body'] as $tr ) { echo '<tr>';
            foreach ( $tr as $td ) { echo '<td>'; echo $td['c']; echo '</td>'; } echo '</tr>'; } echo '</tbody>'; echo '</table>'; }
      ?>


      <?php endif; ?>


      <?php endwhile; ?>

        <?php // Navigation ?>

      <?php else : ?>

        <?php // No Posts Found ?>

    <?php endif; ?>
