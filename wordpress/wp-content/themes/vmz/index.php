<?php get_header() ?>


<section class="article">
  <div class="container">
    <div class="content__wrap">

      <div class="content">
        <div class="title">Пресса о нас</div>
        <ul class="news__list">
          	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
              <li class="news__item">
                <a class="news__link" href="<?php  the_permalink();?>">
                  <div class="news__img" style="background-image:url('<?php echo thumb_or(); ?>')"> </div>
                  <div class="news__date"><?php the_date('d.m.y') ?></div>
                  <div class="news__title"><span><?php the_title();?></span> </div>
                </a>
              </li>
          	<?php endwhile; endif; ?>
        </ul>

        <div class="pagination">
          <?php echo paginate_links( $args ); ?>
        </div>
      </div>

      <aside class="aside">
        <div class="subtitle">О заводе</div>
        <?php
          $about = array(
            'theme_location'  => 'about_menu',
            'container'       => none,
            'items_wrap'      => '<ul class=aside__list>%3$s</ul>',
          );
          wp_nav_menu( $about );
        ?>
      </aside>

    </div>
  </div>
</section>


<?php get_footer() ?>
