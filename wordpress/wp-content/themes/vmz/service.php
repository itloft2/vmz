<?php /* Template name: Услуги */?>

<?php get_header() ?>

<article class="article dark">

  <section class="section">
    <div class="container">
      <div class="title"><?php the_title(); ?></div>
      <?php if (have_posts()) : while (have_posts()) : the_post();?>
        <div class="text-duo"><?php the_content(); ?></div>
    	<?php endwhile; endif;?>
    </div>
  </section>

<?php $service = get_field('serivces'); if(!empty($service)) : ?>
  <section class="service">
    <div class="container">
      <div class="title"> <?php echo get_field('title') ?></div>
      <ul class="service__list">

        <?php foreach ($service as $item): ?>
          <li class="service__item" style="background-image: url('<?php echo $item['cover']['url']; ?>')">
            <div class="service__block">
              <div class="service__title"><?php echo $item['subtitle'] ?></div>
              <div class="text"><?php echo $item['text'] ?></div>
            </div>
          </li>
        <?php endforeach; ?>
        
      </ul>
    </div>
  </section>
<?php endif; ?>

</article>


<?php get_footer(); ?>
