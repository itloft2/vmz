<?php get_header(); ?>

<section class="banner">
  <?php  $bn_image = get_field('image'); ?>
  <div class="banner__block" style="background-image: url('<?php if($bn_image){ echo $bn_image['url']; }else{echo "/img/bg/bg-3.png";}?>')">
    <div class="container">
      <div class="banner__text"><?php the_title(); ?></div>
    </div>
  </div>
</section>


<section class="section">
  <div class="container">
    <div class="content__wrap">
      <div class="content">
        <?php if (have_posts()) : while (have_posts()) : the_post();?>

          <div class="text"><?php the_content(); ?></div>
          
          <?php get_template_part( 'template-parts/content', get_post_format() );?>
        <?php endwhile; endif; ?>
      </div>

    <aside class="aside">
      <div class="subtitle">Продукция</div>
      <?php
        $catalog = array(
          'theme_location'  => 'catalog',
          'container'       => none,
          'items_wrap'      => '<ul class=aside__list>%3$s</ul>',
        );

        wp_nav_menu( $catalog );
      ?>
    </aside>

    </div>
  </div>
</section>


<?php get_footer(); ?>
