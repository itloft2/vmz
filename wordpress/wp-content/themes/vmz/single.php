<?php get_header() ?>

<?php $fields = get_fields()['base']; ?>
<section class="article">
  <div class="container">
    <div class="content__wrap">

      <div class="content">
        <?php if (have_posts()) : while (have_posts()) : the_post();?>
          <div class="title"><?php the_title() ?> </div>
          <div class="text"><?php the_content(); ?></div>

          <?php get_template_part( 'template-parts/content', get_post_format() );?>

        <?php endwhile; endif; ?>
      </div>

      <aside class="aside">
        <div class="subtitle">О заводе</div>
        <?php
          $about = array(
            'theme_location'  => 'about_menu',
            'container'       => none,
            'items_wrap'      => '<ul class=aside__list>%3$s</ul>',
          );
          wp_nav_menu( $about );
        ?>
      </aside>

    </div>
  </div>
</section>


<?php get_footer() ?>
