<?php

add_theme_support( 'post-thumbnails' );

register_nav_menus( array(
    'main_menu' => __( 'Primary Menu'),
    'about_menu' => __('About Menu'),
    'catalog' => __('Catalog'),
) );

function thumb_or($post = null) {
      $post_thumbnail_id = get_post_thumbnail_id( $post_id );
      $img = wp_get_attachment_image_src($post_thumbnail_id, 'large');
    if(!empty($img))
        return $img[0];
    else
        return "/img/bg/cover.png";
}


// Register Custom Post Type
function products() {

	$labels = array(
		'name'                  =>  'Продукция',
		'singular_name'         =>  'Продукция',
		'menu_name'             =>  'Продукция',
		'name_admin_bar'        =>  'Продукция',
		'all_items'             =>  'Вся продукция',
		'add_new_item'          =>  'Добавить новый продукт',
		'add_new'               =>  'Добавить новую',
		'edit_item'             =>  'Редактировать запись продукции',
		'update_item'           =>  'Обновить',
    'view_item'             =>  'Просмотреть',
    'search_items'          =>  'Найти',
		'not_found'             =>  'Не найден',
    'not_found_in_trash'    => 'Не найден в корзине',
    'featured_image'        => 'Изображение',
    'set_featured_image'    => 'Установить изображение',
    'remove_featured_image' => 'Удалить изображение',
    'use_featured_image'    => 'Использовать изображение',
    'insert_into_item'      => 'Вставить в',
    'uploaded_to_this_item' => 'Загрузить',
    'items_list'            => 'Список',
    'items_list_navigation' => 'Список',
    'filter_items_list'     => 'Фильтр',
	);
	$args = array(
		'label'                 => 'Продукция',
		'description'           => 'Описание продукция',
		'labels'                => $labels,
		'supports'              => array('title', 'editor', 'thumbnail',),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'products', $args );

}
add_action( 'init', 'products', 0 );

//svg
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page('Настройки сайта');
}
