<?php get_header(); ?>


<?php $slides = get_field('banner'); if(!empty($slides)) :  ?>
  <section class="banner">
      <ul class="banner__list">
        <?php foreach($slides as $slide) : ?>
          <li class="banner__item" style="background-image:url('<?php echo $slide['image']['url'];?>')">
            <div class="container">
              <div class="banner__text"><?php echo $slide['text'] ?></div>
            </div>
          </li>
        <?php endforeach; ?>
      </ul>
  </section>
<?php endif; ?>


<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <section class="section">
    <div class="container">
      <div class="title"><?php the_title(); ?></div>
      <div class="text-duo"><?php the_content() ?></div>
    </div>
  </section>
<?php endwhile; endif;?>


<section class="product dark">
  <div class="container">
    <div class="title">Продукция<a class="all-btn" href="<?php echo get_post_type_archive_link( 'products' ); ?>">Вся продукция</a></div>
    <ul class="product__list">
      <?php
        $prod = new WP_Query(array(
          'post_type' => 'products',
          'post_status' => 'publish',
          'posts_per_page' => 6,
        ));
      ?>

      <?php if($prod->have_posts()):
              while($prod->have_posts()): $prod->the_post(); ?>
        <li class="product__item">
          <a class="product__link" href="<?php  the_permalink();?>" style="background-image: url('<?php echo thumb_or(); ?>')">
            <span class="product__title"><?php the_title();?></span>
          </a>
        </li>
      <?php endwhile; endif; ?>

    </ul>
  </div>
</section>


<section class="news">
  <div class="container">
    <div class="title">Пресса о нас<a class="all-btn" href="<?php echo get_permalink( get_option('page_for_posts') ); ?>">Читать все</a></div>
    <ul class="news__list">

      <?php
        $news = new WP_Query(array(
          'post_type' => 'post',
          'post_status' => 'publish',
          'posts_per_page' => 3,
        ));
      ?>

      <?php
        if($news->have_posts()):
          while($news->have_posts()): $news->the_post(); ?>
        <li class="news__item">
          <a class="news__link" href="<?php  the_permalink();?>">
            <div class="news__img" style="background-image:url('<?php echo thumb_or(); ?>  ')"> </div>
            <div class="news__date"><?php the_date('d.m.y') ?></div>
            <div class="news__title"><span><?php the_title();?></span> </div>
          </a>
        </li>
      <?php endwhile; endif; ?>

    </ul>
  </div>
</section>


<?php get_footer(); ?>
