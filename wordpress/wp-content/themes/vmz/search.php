<?php get_header(); ?>

<article class="article dark">
  <div class="container">
    <section class="result">

      <div class="title">Результаты поиска
        <span class="subtitle">
          "<?php echo esc_html( get_search_query() ); ?>"
        </span>
        <div class="text">(Найдено записей: <?php global $wp_query; echo $wp_query->found_posts ?>)</div>
      </div>

      <ul class="result__list">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <li>
            <a href="<?php the_permalink(); ?>">
              <div class="subtitle"><?php the_title(); ?></div>
              <div class="text"><?php the_excerpt(); ?></div>
            </a>
          </li>
        <?php endwhile; else: ?>
            <li><?php _e('Ничего не найдено.'); ?></li>
        <?php endif; ?>
      </ul>

    </section>
  </div>
</article>

<?php get_footer(); ?>
