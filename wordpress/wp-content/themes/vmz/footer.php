    </main>
    </div>
    <footer class="footer">
      <div class="container">
        <div class="footer__left"><a class="logo" href="/"><img src="/img/icons/logo.svg"></a>
          <p class="copyright">ООО «Выборгский машиностроительный завод»</p>
        </div>
        <div class="footer__middle">
          <?php
            $nav = array(
              'theme_location'  => 'main_menu',
              'container'       => 'nav',
              'container_class'      => 'main-nav',
            );
            wp_nav_menu( $nav );
          ?>
          <a class="download" href="/img/doc/catalog.pdf" target="_blank" >Каталог продукции ВМЗ</a>
        </div>
        <div class="footer__right"><a class="tel" href="tel:<?=the_field('mobile','option'); ?>"><?= the_field('mobile','option'); ?></a>
          <address class="address">
            <?= the_field('address','option'); ?>
          </address><a class="mail" href="mailto:<?= the_field('mail','option'); ?>"><?= the_field('mail','option'); ?></a>
        </div>
      </div>
      <div class="footer__bottom">
        <div class="container">
          <p>© 2016 ООО «ВМЗ»<a href="javascript:;">Политика конфиденциальности</a></p><a class="itloft" href="javascript:;"><img src="/img/icons/itloft_logo.svg" alt="itloft"></a>
        </div>
      </div>
    </footer>
    <script defer="defer" src="/js/vendor/vendor.js"></script>
    <script defer="defer" src="/js/vendor/lg.js"></script>
    <script defer="defer" src="/js/script.js"></script>
    <?php wp_footer(); ?>
  </body>
</html>
