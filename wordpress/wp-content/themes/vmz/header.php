<!DOCTYPE html>
<html lang="ru-RU">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="ru">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>ВМЗ</title>
    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&amp;lang=ru-RU" type="text/javascript"></script>
    <?php wp_head(); ?>
  </head>


  <body>
    <div class="wrapper">
      <header class="header">
        <div class="search">
          <div class="overlay"> </div>
          <div class="container">
            <div class="title">Поиск</div>
            <form action="/" method="get" class="search__form">
              <input class="search__input" type="text" placeholder="Поиск..." name="s">
              <button type="submit" class="search__submit"></button>
            </form>
          </div>

        </div>


        <div class="container">
          <div class="header__row"><a class="logo" href="<?php echo home_url() ?>"><img src="/img/icons/logo.svg"></a>
            <div class="company-name">Выборгский Машиностроительный Завод</div><a class="tel" href="<?= the_field('mobile','option'); ?>"><?= the_field('mobile','option'); ?></a>
          </div>
          <div class="header__nav">
            <?php
              $nav = array(
                'theme_location'  => 'main_menu',
                'container'       => 'nav',
                'container_class' => 'main-nav',
                'items_wrap'      => '<ul>%3$s</ul>',
              );


              wp_nav_menu( $nav );
            ?>
            <a class="search-btn" href="javascript:;">
              <svg xmlns="http://www.w3.org/2000/svg" class="search-icon" width="77.703" height="76.96" viewBox="0 0 77.703 76.96">
                <path d="M56.488 47.362c4.945 4.62 9.836 9.097 14.61 13.695 1.905 1.838 3.856 3.76 5.234 5.984 1.662 2.687 2.243 5.98-.763 8.086-2.778 1.945-6.1 2.873-9.155.066-5.643-5.18-11.37-10.275-16.863-15.61-2.088-2.028-3.62-2.34-6.43-1.075-16.102 7.25-34.125-.174-40.917-16.57-6.34-15.304 1.503-33.687 16.95-39.72C35.602-4.205 53.586 3.77 59.532 20.346c3.343 9.32 2.084 18.35-3.045 27.017zM31.023 11.035c-10.686-.063-19.46 8.478-19.647 19.128-.192 10.865 8.542 19.862 19.36 19.946 10.6.08 19.508-8.586 19.693-19.16.19-10.847-8.584-19.85-19.407-19.915z"/>
              </svg>
            </a>
            <a class="download" href="/img/doc/catalog.pdf" target="_blank">Каталог продукции ВМЗ</a>
          </div>
        </div>
      </header>
      <main class="maincontent">
