<?php get_header(); ?>

<article class="article dark">
  <section class="product">
    <div class="container">
      <div class="title"><?php post_type_archive_title();?></div>
      <div class="text-duo">
        <?= the_field('product_text','option'); ?>
      </div>

      <ul class="product__list">
        <?php query_posts( "post_type=products&posts_per_page=-1" ); ?>
        <?php if (have_posts()) : while (have_posts()) : the_post();?>
          <li class="product__item">
            <a class="product__link" href="<?php  the_permalink();?>" style="background-image: url('<?php echo thumb_or(); ?>')">
              <span class="product__title"><?php the_title();?></span>
            </a>
          </li>
    	  <?php endwhile;  endif;?>
      </ul>

    </div>
  </section>
</article>


<?php get_footer(); ?>
