<?php /* Template name: Контакты */?>

<?php get_header(); ?>

<article class="article dark">
  <section class="contact">
    <div class="container">
      <div class="title"><?php the_title(); ?></div>


      <ul class="contact__list">
        <li class="contact__item">
          <div class="subtitle">Адрес:</div>
          <address class="contact__address"><?= the_field('address','option'); ?></address>
        </li>
        <li class="contact__item">
          <div class="subtitle">Телефоны:</div>
          <p><a href="tel:<?= the_field('tel','option'); ?>"><?= the_field('tel','option'); ?></a></p>
          <p><a href="tel:<?= the_field('mobile','option'); ?>"><?= the_field('mobile','option'); ?></a></p>
        </li>
        <li class="contact__item">
          <div class="subtitle">Режим работы:</div>
          <p><?= the_field('mode','option'); ?></p>
        </li>
        <li class="contact__item">
          <div class="subtitle">E-mail:</div>
          <p><a href="mailto:<?= the_field('mail','option'); ?>"><?= the_field('mail','option'); ?></a></p>
        </li>
      </ul>
    </div>
  </section>
  <section class="maps">
    <div id="map"></div>
  </section>
  <section class="section">
    <div class="container">
      <div class="title">Задать вопрос</div>

      <?php echo do_shortcode( '[contact-form-7 id="195" title="ask"]' ); ?>

    </div>
  </section>
</article>


<?php get_footer(); ?>
